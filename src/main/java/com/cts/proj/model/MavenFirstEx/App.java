package com.cts.proj.model.MavenFirstEx;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.cts.proj.model1.Employee1;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       ApplicationContext ctx=new ClassPathXmlApplicationContext("configure.xml");
       Employee1 emp=(Employee1)ctx.getBean("emp1");
       emp.display();
    }
}
